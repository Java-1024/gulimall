package com.liao.gulimall.ware.dao;

import com.liao.gulimall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author liaojinhu
 * @email 153038066@qq.com
 * @date 2021-07-23 18:45:26
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
