package com.liao.gulimall.ware.dao;

import com.liao.gulimall.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author liaojinhu
 * @email 153038066@qq.com
 * @date 2021-07-23 18:45:26
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
