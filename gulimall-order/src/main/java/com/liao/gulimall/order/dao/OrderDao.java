package com.liao.gulimall.order.dao;

import com.liao.gulimall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author liaojinhu
 * @email 153038066@qq.com
 * @date 2021-07-23 18:43:00
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
