package com.liao.gulimall.product.dao;

import com.liao.gulimall.product.entity.BrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 品牌
 * 
 * @author liaojinhu
 * @email 153038066@qq.com
 * @date 2021-07-23 15:10:00
 */
@Mapper
public interface BrandDao extends BaseMapper<BrandEntity> {
	
}
