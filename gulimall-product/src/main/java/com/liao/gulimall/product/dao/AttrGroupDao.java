package com.liao.gulimall.product.dao;

import com.liao.gulimall.product.entity.AttrGroupEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 属性分组
 * 
 * @author liaojinhu
 * @email 153038066@qq.com
 * @date 2021-07-23 15:09:59
 */
@Mapper
public interface AttrGroupDao extends BaseMapper<AttrGroupEntity> {
	
}
