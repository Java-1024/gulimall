package com.liao.gulimall.product.controller;

import java.util.Arrays;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.liao.gulimall.product.entity.SpuCommentEntity;
import com.liao.gulimall.product.service.SpuCommentService;
import com.liao.common.utils.PageUtils;
import com.liao.common.utils.R;



/**
 * 商品评价
 *
 * @author liaojinhu
 * @email 153038066@qq.com
 * @date 2021-07-23 16:09:40
 */
@RestController
@RequestMapping("product/spucomment")
public class SpuCommentController {
    @Autowired
    private SpuCommentService spuCommentService;

    /**
     * 列表
     */
    @RequestMapping("/list")
   // @RequiresPermissions("product:spucomment:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = this.spuCommentService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
   // @RequiresPermissions("product:spucomment:info")
    public R info(@PathVariable("id") Long id){
		SpuCommentEntity spuComment = this.spuCommentService.getById(id);

        return R.ok().put("spuComment", spuComment);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
   // @RequiresPermissions("product:spucomment:save")
    public R save(@RequestBody SpuCommentEntity spuComment){
        this.spuCommentService.save(spuComment);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
   // @RequiresPermissions("product:spucomment:update")
    public R update(@RequestBody SpuCommentEntity spuComment){
        this.spuCommentService.updateById(spuComment);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("product:spucomment:delete")
    public R delete(@RequestBody Long[] ids){
        this.spuCommentService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
