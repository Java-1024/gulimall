package com.liao.gulimall.product.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.liao.gulimall.product.entity.CategoryEntity;
import com.liao.gulimall.product.service.CategoryService;
import com.liao.common.utils.R;


/**
 * 商品三级分类
 *
 * @author liaojinhu
 * @email 153038066@qq.com
 * @date 2021-07-23 16:09:39
 */
@RestController
@RequestMapping("product/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    /**
     * 获取树形结构商品分类
     */
    @RequestMapping("/list/tree")
    public R getCategoryforTree() {
        List<CategoryEntity> categoryEntities = this.categoryService.getListToTree();

        return R.ok().put("data", categoryEntities);
    }

//    /**
//     * 列表
//     */
//    @RequestMapping("/list")
//   // @RequiresPermissions("product:category:list")
//    public R list(@RequestParam Map<String, Object> params){
//        PageUtils page = this.categoryService.queryPage(params);
//
//        return R.ok().put("page", page);
//    }


    /**
     * 信息
     */
    @RequestMapping("/info/{catId}")
    // @RequiresPermissions("product:category:info")
    public R info(@PathVariable("catId") Long catId) {
        CategoryEntity category = this.categoryService.getById(catId);

        return R.ok().put("data", category);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("product:category:save")
    public R save(@RequestBody CategoryEntity category) {
        this.categoryService.save(category);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("product:category:update")
    public R update(@RequestBody CategoryEntity category) {
        this.categoryService.updateById(category);

        return R.ok();
    }

    @RequestMapping("/update/sort")
    // @RequiresPermissions("product:category:update")
    public R updateSort(@RequestBody CategoryEntity[] category) {
        this.categoryService.updateBatchById(Arrays.asList(category));

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("product:category:delete")
    public R delete(@RequestBody Long[] catIds) {
        //this.categoryService.removeByIds(Arrays.asList(catIds));

        //1：检查当前删除的菜单，是否被其他地方引用
//		categoryService.removeByIds(Arrays.asList(catIds));
        //Arrays.asList()该方法是将数组转化成List集合的方法。
        this.categoryService.removeMenuByIds(Arrays.asList(catIds));

        return R.ok();
    }

}
