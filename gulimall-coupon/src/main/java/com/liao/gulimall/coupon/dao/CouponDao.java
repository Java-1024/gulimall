package com.liao.gulimall.coupon.dao;

import com.liao.gulimall.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author liaojinhu
 * @email 153038066@qq.com
 * @date 2021-07-23 18:35:57
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
