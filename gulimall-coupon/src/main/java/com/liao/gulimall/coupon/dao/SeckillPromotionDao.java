package com.liao.gulimall.coupon.dao;

import com.liao.gulimall.coupon.entity.SeckillPromotionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 秒杀活动
 * 
 * @author liaojinhu
 * @email 153038066@qq.com
 * @date 2021-07-23 18:35:57
 */
@Mapper
public interface SeckillPromotionDao extends BaseMapper<SeckillPromotionEntity> {
	
}
